package Zaaksysteem::OLO::Message;

# Provide for __SUB__ current sub reference
use feature qw[current_sub];

use Moose;
use namespace::autoclean;

=head1 NAME

Zaaksysteem::OLO::Message - OLO XML Message data holder

=head1 DESCRIPTION

This class abstracts the notion of an OLO message.

=cut

use BTTW::Tools;
use List::Util qw[any uniq];
use XML::LibXML ();

=head1 ATTRIBUTES

=head2 content

Bare XML content of the message.

=cut

has content => (
    is => 'rw',
    isa => 'Str',
    lazy => 1,
    builder => '_resolve_content',
);

=head2 timestamp

Creation timestamp for the message, as reported by the origin repository.

=cut

has timestamp => (
    is => 'rw',
    isa => 'DateTime',
    required => 1
);

=head2 message_namespaces

Contains a map of known namespaces for OLO XML messages.

=cut

has message_namespaces => (
    is => 'rw',
    isa => 'HashRef[Str]',
    traits => [qw[Hash]],
    builder => '_build_message_namespaces',
    handles => {
        message_namespace_keys => 'keys',
        get_message_namespace => 'get'
    }
);

=head2 document

Lazy-built L<XML::LibXML::Document> object.

=cut

has document => (
    is => 'rw',
    isa => 'XML::LibXML::Document',
    lazy => 1,
    builder => '_build_document'
);

=head2 xpc

Lazy-built L<XML::LibXML::XPathContext> object.

=cut

has xpc => (
    is => 'rw',
    isa => 'XML::LibXML::XPathContext',
    lazy => 1,
    builder => '_build_xpc'
);

=head1 PRIVATE ATTRIBUTES

=head2 _content_resolver

Coreref that should resolve the content of the message.

=head3 Delegates

=over 4

=item _resolve_content

Executes the code to resolve the content.

=back

=cut

has _content_resolver => (
    is => 'rw',
    isa => 'CodeRef',
    traits => [qw[Code]],
    default => sub {
        return sub {
            throw(
                'olo/message/content_resolver_undefined',
                'Message "%s" configured without content resolver'
            )
        };
    },
    handles => {
        _resolve_content => 'execute'
    }
);

=head1 METHODS

=head2 get_xpath_context

Parses the L</content> string and extracts the actual message.

=cut

sub get_xpath_context {
    my $self = shift;

    my $xc = XML::LibXML::XPathContext->new($self->document);

    for my $key ($self->message_namespace_keys) {
        $xc->registerNs($key, $self->get_message_namespace($key));
    }

    return $xc;
}

=head2 is_valid

Verifies the XML 'looks like' our expected format (LVO 0312)

=cut

sub is_valid {
    my $self = shift;

    my $flatten = sub {
        return (
            (map { $_->declaredURI } map { $_->getNamespaces } @_),
            (map { __SUB__->($_->childNodes) } @_)
        )
    };

    my $lvo_uri = $self->get_message_namespace('lvo');

    return any {
        $_ eq $lvo_uri
    } uniq $flatten->($self->document->documentElement);
}

=head2 stuf_reference

Returns the reference number of the StUF message exchange from the
C<//stuurgegevens/referentienummer> node.

=cut

sub stuf_reference {
    return shift->xpc->findvalue('//lvo:stuurgegevens/stuf:referentienummer');
}

=head2 stuf_type

Returns the value of the C<//stuurgegevens/functie> node.

=cut

sub stuf_type {
    return shift->xpc->findvalue('//lvo:stuurgegevens/stuf:functie');
}

=head2 application_id

Returns the value of the C<//object/aanvraagGegevens/aanvraagnummer> node.

=cut

sub application_id {
    my $self = shift;

    return $self->xpc->findvalue(
        'lvo:aanvraagGegevens/lvo:aanvraagnummer',
        $self->_object_node
    );
}

=head2 application_attributes

Returns a hash of attribute name/value pairs from
C<//object/aanvraagGegevens>.

=cut

sub application_attributes {
    my $self = shift;

    my @attribute_nodes = $self->xpc->findnodes(
        'lvo:aanvraagGegevens/*',
        $self->_object_node
    );

    return unless scalar @attribute_nodes;

    return {
        map { $_->localname => $_->textContent } @attribute_nodes
    };
}

=head2 attachment_paths

Returns a list of document paths for the message's attachments from the
C<//object/heeftBijlage> elementset.

    (
        '/path/to/the/file.yiff',
        '/and/another.pdf'
    )

=cut

sub attachment_paths {
    my $self = shift;

    my @attachment_nodes = $self->xpc->findnodes('lvo:heeftBijlage', $self->_object_node);

    return map {
        $self->xpc->findvalue('lvo:gerelateerde/lvo:bestandsnaam', $_)
    } @attachment_nodes;
}

=head2 location

Attempts to construct a set of location attributes and values for the
application from the C<//object/isVoor/gerelateerde/isAdres> elementset.

Returns a simple hashref structure with the data if an address was found,
C<undef> otherwise.

    {
        city => '...',
        street => '...',
        street_number => '...',
        postal_code => '...'
    }

=cut

sub location {
    my $self = shift;

    my ($address_node) = $self->xpc->findnodes(
        'lvo:isVoor/lvo:gerelateerde/lvo:isAdres',
        $self->_object_node
    );

    return unless defined $address_node;

    my $prefix = 'lvo:gerelateerde/bg:adresAanduidingGrp';

    my %map = (
        city => "$prefix/bg:wpl.woonplaatsNaam",
        street => "$prefix/bg:gor.straatnaam",
        street_number => "$prefix/bg:aoa.huisnummer",
        postal_code => "$prefix/bg:aoa.postcode"
    );

    my $location = {
        map {
            $_ => $self->xpc->findvalue($map{ $_ }, $address_node)
        } keys %map
    };

    return $location;
}

=head2 requestor

Attempts to find the application's requestor and returns the primary
identifier and type. Returns C<undef> if no requestor could be found.

Uses the C<//object/isAangevraagdDoor> elementset.

Person:

    {
        type => 'person',
        id => '123456789'
    }

Company:

    {
        type => 'company',
        id => {
            kvk_number => '1232345',
            branch_number => '6575445'
        }
    }

=cut

sub requestor {
    my $self = shift;

    my ($requestor_node) = $self->xpc->findnodes(
        'lvo:isAangevraagdDoor/lvo:gerelateerde',
        $self->_object_node
    );

    return unless defined $requestor_node;

    if ($self->xpc->exists('bg:natuurlijkPersoon', $requestor_node)) {
        return {
            type => 'person',
            id => $self->xpc->findvalue(
                'bg:natuurlijkPersoon/bg:inp.bsn',
                $requestor_node
            )
        };
    }

    if ($self->xpc->exists('bg:kvkNummer', $requestor_node)) {
        return {
            type => 'company',
            id => {
                kvk_number => $self->xpc->findvalue(
                    'bg:kvkNummer',
                    $requestor_node
                ),
                branch_number => $self->xpc->findvalue(
                    'bg:vestiging/bg:vestigingsNummer',
                    $requestor_node
                )
            }
        };
    }

    return;
}

=head1 PRIVATE METHODS

=head2 _object_node

Returns a L<XML::LibXML::Element> representing the
C<http://www.egem.nl/StUF/sector/lvo/0312:object> node.

We assume there is only one update in every message, and only one object in
every update.

=cut

sub _object_node {
    return (shift->xpc->findnodes('//lvo:update/lvo:object'))[0];
}

=head2 _build_message_namespaces

Builds an array of namespaces used in OLO messages

=cut

sub _build_message_namespaces {
    my $self = shift;

    return {
        bi     => 'http://www.be-informed.nl/BeInformed',
        biattr => 'http://www.be-informed.nl/BeInformed/Attributes',
        soap   => 'http://schemas.xmlsoap.org/soap/envelope/',
        stuf   => 'http://www.egem.nl/StUF/StUF0301',
        bg     => 'http://www.egem.nl/StUF/sector/bg/0310',
        lvo    => 'http://www.egem.nl/StUF/sector/lvo/0312',
        zkn    => 'http://www.egem.nl/StUF/sector/zkn/0310',
        w3a    => 'http://www.w3.org/2005/08/addressing',
        w3s    => 'http://www.w3.org/2001/XMLSchema-instance',
    };
}

=head2 _build_xpc

Builds a L<XML::LibXML::XPathContext> instance for L</xpc>.

=cut

sub _build_xpc {
    my $self = shift;

    return $self->get_xpath_context;
}

=head2 _build_document

Builds a L<XML::LibXML::Document> instance for L</document>.

=cut

sub _build_document {
    my $self = shift;

    my $doc = XML::LibXML->load_xml(string => $self->content);

    unless (eval { $doc->isa('XML::LibXML::Document') }) {
        throw('olo/message/xml_load_failed', sprintf(
            'Caught exception while loading XML message: %s',
            $@
        ));
    }

    return $doc;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE> file.
