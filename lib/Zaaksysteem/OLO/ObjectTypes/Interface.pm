package Zaaksysteem::OLO::ObjectTypes::Interface;

use Moose;
use namespace::autoclean;

extends 'Syzygy::Object::Type::Moose';

=head1 NAME

Zaaksysteem::OLO::ObjectTypes::Interface - Simple representation of
Zaaksysteem C<interface> instances.

=head1 DESCRIPTION

Zaaksysteem uses C<interface> objects to communicate configuration for
system integrations. This type allows OLO to use these objects.

=cut

use BTTW::Tools;
use DateTime::Format::ISO8601;
use Syzygy::Syntax;

=head1 OBJECT TYPE ATTRIBUTES

=head2

=cut

szg_attr id => (
    value_type_name => 'string'
);

szg_attr module => (
    value_type_name => 'string'
);

szg_attr name => (
    value_type_name => 'text'
);

szg_attr casetype_id => (
    value_type_name => 'uuid'
);

szg_attr interface_config => (
    value_type_name => 'complex'
);

=head1 METHODS

=head2 get_attribute_map

Returns the attribute mapping for the interface. Maps C<LVO>
C<aanvraagGegevens> element names to Zaaksysteem attribute names.

=cut

sub get_attribute_map {
    my ($type, $instance) = @_;

    my $config = $instance->get_value('interface_config')->value;

    my %map = map {
        $_->{ external_name } => $_->{ internal_name }{ searchable_object_id }
    } @{ $config->{ attribute_mapping } };

    return \%map;
}

=head2 get_default_requestors

Returns a map of default requestor identifierobjects (which can be
directly used in a case/create API call).

=cut

sub get_default_requestors {
    my ($type, $instance) = @_;

    my $config = $instance->get_value('interface_config')->value;

    my %default_requestors;

    if (my $np = $config->{ fallback_natuurlijk_persoon }) {
        $default_requestors{ person } = {
            type => 'person',
            id => $np->{ burgerservicenummer }
        };
    }

    if (my $c = $config->{ fallback_bedrijf }) {
        $default_requestors{ company } = {
            type => 'company',
            id => {
                kvk_number => $c->{ dossiernummer },
                branch_number => $c->{ vestigingsnummer }
            }
        }
    }

    return \%default_requestors;
}

=head2 get_last_sync

Returns a L<DateTime> instance of the last time the synchronisation was run
according to the interface.

=cut

sub get_last_sync {
    my ($type, $instance) = @_;

    my $config = $instance->get_value('interface_config')->value;

    return unless $config->{ last_sync };

    return DateTime::Format::ISO8601->parse_datetime($config->{ last_sync });
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
