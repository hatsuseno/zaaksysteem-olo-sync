package Zaaksysteem::OLO::ObjectTypes::Exception;

use Moose;
use namespace::autoclean;

extends 'Syzygy::Object::Type::Moose';

=head1 NAME

Zaaksysteem::OLO::ObjectTypes::Exception - Simplified object type
for C<exception> instances.

=head1 DESCRIPTION

This class declares the type for C<exception> instances. Deprecated when
L<Syzygy> gets the type.

=cut

use Syzygy::Syntax;

=head1 OBJECT TYPE ATTRIBUTES

=head2 type

Contains a type specifier for the exception.

=cut

szg_attr type => (
    value_type_name => 'string'
);

=head2 message

Contains a textual message describing the error.

=cut

szg_attr message => (
    value_type_name => 'text'
);

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
