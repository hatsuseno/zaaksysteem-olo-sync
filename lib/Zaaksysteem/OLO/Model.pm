package Zaaksysteem::OLO::Model;

use Moose;
use namespace::autoclean;

with qw[
    MooseX::Log::Log4perl
];

=head1 NAME

Zaaksysteem::OLO::Model - OmgevingsLoketOnline message processor

=head1 DESCRIPTION

This model abstracts OmgevingsLoketOnline synchronisation with Zaaksysteem.

=head1 SYNOPSIS

    my $model = Zaaksysteem::OLO::Model->new(
        api_client => Zaaksysteem::API::Client->new(...),
        repository => Zaaksysteem::OLO::Repository::Local->new(...),
        attribute_name_map => {
            aanvraagnummer => 'application_id',
            foo => 'bar',
            ...
        }
    );

    # Synchronize last 24 hours of messages
    $model->sync(DateTime->now->subtract(days => 1));

=cut

use BTTW::Tools;
use List::Util qw[first];
use Moose::Util::TypeConstraints qw[role_type];
use Syzygy::Object::Model;
use Syzygy::Types qw[UUID];
use Time::HiRes qw[gettimeofday tv_interval];

# Add our object type library to the running syzygy model
Syzygy::Object::Model->get_instance->load_object_type_modules(
    'Zaaksysteem::OLO::ObjectTypes'
);

=head1 ATTRIBUTES

=head2 api_client

References a L<Zaaksysteem::API::Client> instance which is used as a target
for mutations derived from OLO messages.

=cut

has api_client => (
    is => 'rw',
    isa => 'Zaaksysteem::API::Client',
    required => 1
);

=head2 repository

Reference to an instance of a class implementing
L<Zaaksysteem::OLO::Interface::Repository>.

The repository is used to retrieve messages and attachments.

=cut

has repository => (
    is => 'rw',
    isa => role_type('Zaaksysteem::OLO::Interface::Repository'),
    required => 1
);

=head2 attribute_name_map

Attribute name mapping configuration. The model uses this mapping to rewrite
a message's XML element names to the values in this hash.

=cut

has attribute_name_map => (
    is => 'rw',
    isa => 'HashRef[Maybe[Str]]',
    traits => [qw[Hash]],
    required => 1,
    handles => {
        has_attribute_name => 'exists',
        get_attribute_name => 'get'
    }
);

=head2 casetype_id

References the C<casetype> object for which the messages should be processed.

=cut

has casetype_id => (
    is => 'rw',
    isa => UUID,
    required => 1
);

=head2 default_requestors

Map of default requestors for case creation if the subject specified in a
message cannot be found (using a try-and-again-strategy).

=cut

has default_requestors => (
    is => 'rw',
    isa => 'HashRef',
    traits => [qw[Hash]],
    default => sub { return {} },
    handles => {
        has_default_requestor => 'exists',
        get_default_requestor => 'get',
        set_default_requestor => 'set'
    }
);

=head1 METHODS

=head2 sync

Performs a synchronisation run.

    # Synchronize all messages
    $model->sync;

    # Synchronize messages added in the last week
    $model->sync(DateTime->now->subtract(days => 7));

=cut

sig sync => '?DateTime';

sub sync {
    my $self = shift;
    my $since = shift;

    my $t_start = [ gettimeofday ];

    $self->log->info(sprintf(
        'Starting OmgevingsLoketOnline synchronisation (%s)',
        $since ? ('since ' . $since->iso8601) : 'all messages'
    ));

    for my $message ($self->repository->get_messages($since)) {
        try {
            $self->process_message($message);
        } catch {
            $self->log->warn(sprintf(
                'Processing message (%s: %s) failed: %s',
                $message->stuf_type,
                $message->stuf_reference,
                $_
            ));
        };
    }

    $self->log->info(sprintf(
        'Finished OmgevingsLoketOnline synchronisation (took %.2f seconds)',
        tv_interval($t_start)
    ));

    return;
}

=head2 process_message

Takes a L<Zaaksysteem::OLO::Message> instance and attempts to process it.

Currently C<IndienenAanvulling> and C<AanbiedenAanvraag> are the supported
message types.

    $model->process_message($my_message);

=cut

sig process_message => 'Zaaksysteem::OLO::Message';

sub process_message {
    my $self = shift;
    my $message = shift;

    my %dispatch = (
        IndienenAanvulling => sub { return $self->_process_update_message(@_) },
        AanbiedenAanvraag  => sub { return $self->_process_create_message(@_) }
    );

    unless ($message->is_valid) {
        throw('olo/sync/process_message/invalid', sprintf(
            'Message did not validate (is the XML valid LVO-0312?)'
        ), $message);
    }

    my $processor = $dispatch{ $message->stuf_type };

    unless (defined $processor) {
        throw('olo/sync/process_message/type_unsupported', sprintf(
            'Unsupported message type "%s" for application "%s"',
            $message->stuf_type,
            $message->stuf_reference
        ));
    }

    $self->log->info(sprintf(
        'Processing %s message "%s" for application_id "%s"',
        $message->stuf_type,
        $message->stuf_reference,
        $message->application_id
    ));

    return $processor->($message);
}

=head2 get_case_by_application_id

Given a L<Zaaksysteem::OLO::Message/application_id>, attempts to use the
L</api_client> to retrieve a C<case> object associated with the id.

The returned case will be a L<Syzygy::Object> instance with appropriate type.

    my $case = $model->get_case_by_application_id(123);

=cut

sub get_case_by_application_id {
    my $self = shift;
    my $id = shift;

    my $zql = sprintf(
        'SELECT {} FROM case WHERE (case.casetype = "%s") AND (attribute.%s = "%s")',
        $self->casetype_id,
        $self->map_attribute_name('aanvraagnummer'),
        $id
    );

    my $res = $self->api_client->get('case', { zql => $zql });

    unless ($res->http_status eq '200') {
        throw('olo/sync/case_retrieval_error', sprintf(
            'Unexpected error while querying "%s": %s',
            $zql,
            $res->request_id
        ));
    }

    my $set = Syzygy::Object::Model->read_graph_hash($res->data);

    unless ($set->type_name eq 'set') {
        throw('olo/sync/invalid_api_reponse', sprintf(
            'Unexpected object in query response, expected a set, got a "%s": %s',
            $res->data->{ type },
            $res->request_id
        ));
    }

    my $rows = $set->get_value('rows');

    unless (defined $rows) {
        throw('olo/sync/rows_missing', sprintf(
            'Set of cases returned in request "%s" is missing',
            $res->request_id
        ));
    }

    my @results = @{ $rows->value };

    if (scalar @results > 1) {
        throw('olo/sync/multiple_cases_found', sprintf(
            'Query for case with application_id "%s" gave multiple results',
            $id
        ));
    }

    my $case_hash = shift @results;

    # no results, clean return (not neccesarily a failure condition)
    return unless defined $case_hash;

    my $case = try {
        return Syzygy::Object::Model->read_graph_hash($case_hash);
    } catch {
        throw('olo/sync/case_inflation_failed', sprintf(
            'Caught exception while inflating case: %s',
            $_
        ));
    };

    unless ($case->type_name eq 'case') {
        throw('olo/sync/expected_case_instance', sprintf(
            'Expected a "case" instance in API response, but got a "%s"',
            $case->type_name
        ));
    }

    return $case;
}

=head2 map_attribute_name

Maps a object attribute name using L</attribute_name_map>.

Defaults to the provided name, as fallback behavior for environments where the
magic strings match those of the interface mapping.

=cut

sig map_attribute_name => 'Str => Str';

sub map_attribute_name {
    my $self = shift;
    my $name = shift;

    return $self->get_attribute_name($name) || $name;
}

=head1 PRIVATE METHODS

=head2 _process_update_message

Called by L</process_message> when a C<IndienenAanvulling> message is found.

Posts new application attachments to an existing C<case> object in the target
environment (via L</api_client>).

=cut

sub _process_update_message {
    my $self = shift;
    my $message = shift;

    my $attachments_attribute_name = $self->map_attribute_name('bijlagen');

    unless (defined $attachments_attribute_name) {
        $self->log->info(sprintf(
            'Attachment attribute not mapped, skipping update'
        ));

        return;
    }

    my $case = $self->get_case_by_application_id($message->application_id);

    unless (defined $case) {
        throw('olo/sync/process_update/case_not_found', sprintf(
            'Message could not be processed, case not found'
        ));
    }

    my $filebag = $self->_upload_message_attachments($message);

    unless (defined $filebag) {
        $self->log->warn(sprintf(
            'No attachments uploaded for %s message "%s", skipping',
            $message->stuf_type,
            $message->stuf_reference
        ));

        return;
    }

    my @filestore_ids = keys %{ $filebag->get_value('references')->value };

    $self->log->info(sprintf('Updating case "%s"', $case->id));

    my $res = $self->api_client->post(sprintf('case/%s', $case->id), {
        $attachments_attribute_name => \@filestore_ids
    });

    unless ($res->is_success) {
        throw('olo/sync/process_update/case_update_failed', sprintf(
            'Failed to update case "%s": %s',
            $case->id,
            dump_terse($res->data)
        ));
    }

    return;
}

=head2 _process_create_message

Called by L</process_message> when a L<AanbiedenAanvraag> message is found.

Creates a new C<case> object for the given
L<Zaaksysteem::OLO::Message/application_id> after checking the application
does not exist in the target environment.

=cut

sub _process_create_message {
    my $self = shift;
    my $message = shift;

    my $case = $self->get_case_by_application_id($message->application_id);
    my $attachments_attribute_name = $self->map_attribute_name('bijlagen');

    if (defined $case) {
        throw('olo/sync/process_create/case_found', sprintf(
            'Attempted to create application "%s", but case already exists',
            $message->application_id
        ));
    }

    my $attributes = $message->application_attributes;

    my $filebag = $self->_upload_message_attachments($message);

    my %values = map {
        $self->map_attribute_name($_) => [ $attributes->{ $_ } ]
    } keys %{ $attributes };

    if (defined $filebag && defined $attachments_attribute_name) {
        $values{ $attachments_attribute_name } = [
            keys %{ $filebag->get_value('references')->value }
        ];
    }

    $self->log->info(sprintf(
        'Creating case for application_id "%s"',
        $message->application_id
    ));

    my $requestor = $message->requestor;

    my $res = $self->api_client->post('case/create', {
        casetype_id => $self->casetype_id,
        source => 'email',
        values => \%values,
        requestor => $requestor
    });

    return if $res->is_success;

    # API request will except if body is not JSON, so if we even get here
    # we can assume the embedded object is an exception.
    my $exception = Syzygy::Object::Model->read_graph_hash($res->data);

    # The referenced subject could not be found, so we try again with the
    # default requestor.
    if ($exception->get_value('type')->value eq 'case/get_subject') {
        my $default_requestor = $self->get_default_requestor($requestor->{ type });

        if (defined $default_requestor) {
            $self->log->info('Failed to create case for supplied requestor, using fallback');

            $res = $self->api_client->post('case/create', {
                casetype_id => $self->casetype_id,
                source => 'email',
                values => \%values,
                requestor => $default_requestor
            });
        }
    }

    return if $res->is_success;

    throw('olo/sync/process_update/case_update_failed', sprintf(
        'Failed to create case for application_id "%s": %s',
        $message->application_id,
        dump_terse($res->data)
    ));
} 

=head2 _upload_message_attachments

Given a L<Zaaksysteem::OLO::Message> this helper method will attempt to upload
the message's attachments to C</case/prepare_file> so the files can be
associated with a case.

Returns a L<Syzygy::Object> of the L<Zaaksysteem::OLO::ObjectTypes::Filebag>
type, or throws an exception.

=cut

sub _upload_message_attachments {
    my $self = shift;
    my $message = shift;
    my %attachments;

    my $application_id = $message->application_id;

    for my $path ($message->attachment_paths) {
        my $name = File::Basename::fileparse($path);

        $attachments{ $name } = $self->repository->get_file($path);

        $self->log->info(sprintf(
            'Preparing upload of document "%s" for application_id "%s"',
            $name,
            $application_id
        ));
    }

    # No files to upload, move along.
    return unless scalar keys %attachments;

    my $req = $self->api_client->build_form_request('case/prepare_file', {
        map { $_ => [ $_, $attachments{ $_ } ] } keys %attachments
    });

    my $res = $self->api_client->request($req);

    unless ($res->is_success) {
        throw('olo/sync/process_update/file_upload_failed', sprintf(
            'File upload for message (%s: %s) failed, received status code %s',
            $message->stuf_type,
            $message->stuf_reference,
            $res->http_status
        ), $res);
    }

    my $filebag = try { 
        return Syzygy::Object::Model->read_graph_hash(
            $res->data
        );
    } catch {
        throw('olo/sync/upload_attachments/filebag_inflation_error', sprintf(
            'Expected a "filebag" object in file upload response, but inflation failed: %s',
            $_
        ));
    };

    unless ($filebag->type_name eq 'filebag') {
        throw('olo/sync/upload_attachments/filebag_expected', sprintf(
            'Expected a "filebag" object in file upload response, but got a "%s"',
            $filebag->type_name
        ));
    }

    return $filebag;
}

__PACKAGE__->meta->make_immutable;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
