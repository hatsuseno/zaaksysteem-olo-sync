package Zaaksysteem::Test::OLO::Model;

use Test::Class::Moose;
use Test::Fatal;
use Test::Mock::One;

use Syzygy::Object::Model;
use Zaaksysteem::API::Response;
use Zaaksysteem::OLO::Model;
use Zaaksysteem::OLO::Message;
use Zaaksysteem::OLO::ObjectTypes::SimpleCase;
use Zaaksysteem::OLO::ObjectTypes::Filebag;

use FindBin;
use UUID::Tiny qw[create_uuid_as_string UUID_V4];

sub test_olo_model_init {
    my $model = Syzygy::Object::Model->get_instance;

    my %test_map = (
        case => 'Zaaksysteem::OLO::ObjectTypes::SimpleCase',
        filebag => 'Zaaksysteem::OLO::ObjectTypes::Filebag',
        interface => 'Zaaksysteem::OLO::ObjectTypes::Interface'
    );

    isa_ok $model->get_object_type($_), $test_map{ $_ }, "object type $_ loaded ok" for keys %test_map;
}

sub test_olo_update_message {
    my $last_get_request;
    my $last_post_request;

    my $case_hash = {
        type => 'case',
        reference => 'f08b7a09-3945-42ac-a620-9d1d3f963868',
        instance => {
            id => 'f08b7a09-3945-42ac-a620-9d1d3f963868',
            attributes => {
                foo => ['abc']
            }
        }
    };

    my $set_hash = {
        type => 'set',
        instance => {
            rows => [ $case_hash ]
        }
    };

    my $mock_api = Test::Mock::One->new(
        'X-Mock-ISA' => 'Zaaksysteem::API::Client',
        build_form_request => sub {
            return \@_;
        },
        request => sub {
            my ($path, $attachments) = @{ shift() };

            return Zaaksysteem::API::Response->new(
                request_id => 'abc',
                http_status => 200,
                data => {
                    type => 'filebag',
                    instance => {
                        references => {
                            map {
                                create_uuid_as_string(UUID_V4) => $_
                            } keys %{ $attachments }
                        }
                    }
                }
            );
        },
        get => sub {
            $last_get_request = [ @_ ];
            return Zaaksysteem::API::Response->new(
                request_id => '123',
                data => $set_hash,
                http_status => 200
            );
        },
        post => sub {
            $last_post_request = [ @_ ];
            return Zaaksysteem::API::Response->new(
                request_id => 'xyz',
                data => $set_hash,
                http_status => 200
            );
        }
    );

    my $model = Zaaksysteem::OLO::Model->new(
        api_client => $mock_api,
        casetype_id => create_uuid_as_string(UUID_V4),
        attribute_name_map => {
            aanvraagnummer => 'foo',
            bijlagen => 'bar'
        },
        repository => Zaaksysteem::OLO::Repository::Local->new(
            path => "$FindBin::Bin/data/repo"
        )
    );

    isa_ok $model, 'Zaaksysteem::OLO::Model', 'olo model constructor retval';

    my @messages = $model->repository->get_messages;

    # Grab our update test message
    my ($update_message) = grep { $_->stuf_reference eq '6418126' } @messages;

    lives_ok {
        $model->process_message($update_message)
    } 'processing update message lives';

    subtest update_case_get_request => sub {
        is ref $last_get_request, 'ARRAY', 'last get request set';

        my ($get_path, $get_params) = @{ $last_get_request };

        is $get_path, 'case', 'case resource path used in case retrieval request';
        is ref $get_params, 'HASH', 'case retrieval request has parameters';
        ok exists $get_params->{ zql }, 'case retrieval has zql parameter';
    };

    subtest update_case_post_request => sub {
        is ref $last_post_request, 'ARRAY', 'last post request set';

        my ($post_path, $post_params) = @{ $last_post_request };

        is $post_path, 'case/f08b7a09-3945-42ac-a620-9d1d3f963868',
            'case update resource path references mock case id';
     
        is ref $post_params, 'HASH', 'case update request has parameters';
        ok exists $post_params->{ bar }, 'expected update key found in update';
        is ref $post_params->{ bar }, 'ARRAY', 'update key contains array';
    }; 
}

sub test_olo_create_message {
    my $last_get_request;
    my $last_post_request;

    my $set_hash = {
        type => 'set',
        instance => {
            rows => [ ]
        }
    };

    my $mock_api = Test::Mock::One->new(
        'X-Mock-ISA' => 'Zaaksysteem::API::Client',
        build_form_request => sub {
            return \@_;
        },
        request => sub {
            my ($path, $attachments) = @{ shift() };

            return Zaaksysteem::API::Response->new(
                request_id => 'abc',
                http_status => 200,
                data => {
                    type => 'filebag',
                    instance => {
                        references => {
                            map {
                                create_uuid_as_string(UUID_V4) => $_
                            } keys %{ $attachments }
                        }
                    }
                }
            );
        },
        get => sub {
            $last_get_request = [ @_ ];
            return Zaaksysteem::API::Response->new(
                request_id => '123',
                data => $set_hash,
                http_status => 200
            );
        },
        post => sub {
            $last_post_request = [ @_ ];
            return Zaaksysteem::API::Response->new(
                request_id => 'xyz',
                data => $set_hash,
                http_status => 200
            );
        }
    );

    my $model = Zaaksysteem::OLO::Model->new(
        api_client => $mock_api,
        casetype_id => create_uuid_as_string(UUID_V4),
        attribute_name_map => {
            aanvraagnummer => 'foo',
            bijlagen => 'bar'
        },
        repository => Zaaksysteem::OLO::Repository::Local->new(
            path => "$FindBin::Bin/data/repo"
        )
    );

    isa_ok $model, 'Zaaksysteem::OLO::Model', 'olo model constructor retval';

    # Grab our create test message
    my ($create_message) = grep {
        $_->stuf_reference eq '6584064'
    } $model->repository->get_messages;

    lives_ok {
        $model->process_message($create_message)
    } 'model processed create message';

    subtest create_case_get_request => sub {
        is ref $last_get_request, 'ARRAY', 'last get request set';

        my ($get_path, $get_params) = @{ $last_get_request };

        is $get_path, 'case', 'case resource path used in case retrieval request';
        is ref $get_params, 'HASH', 'case retrieval request has parameters';
        ok exists $get_params->{ zql }, 'case retrieval has zql parameter';
    };

    subtest create_case_post_request => sub {
        is ref $last_post_request, 'ARRAY', 'last post request set';

        my ($post_path, $post_params) = @{ $last_post_request };

        is $post_path, 'case/create',
            'case create resource path ok';
     
        is ref $post_params, 'HASH', 'case update request has parameters';
        ok exists $post_params->{ casetype_id }, 'expected casetype_id key found';
        ok exists $post_params->{ requestor }, 'expected requestor key found';
        ok exists $post_params->{ source }, 'expected source key found';
        ok exists $post_params->{ values }, 'expected values key found';
        is ref $post_params->{ values }, 'HASH', 'values key contains hashref';

        for my $key (keys %{ $post_params->{ values } }) {
            is ref $post_params->{ values }{ $key }, 'ARRAY', "values->$key key contains arrayref";
        }
    };
}

sub test_olo_create_message_exists {
    my $last_get_request;

    my $case_hash = {
        type => 'case',
        reference => 'f08b7a09-3945-42ac-a620-9d1d3f963868',
        instance => {
            id => 'f08b7a09-3945-42ac-a620-9d1d3f963868',
            attributes => {
                foo => ['abc']
            }
        }
    };

    my $set_hash = {
        type => 'set',
        instance => {
            rows => [ $case_hash ]
        }
    };

    my $mock_api = Test::Mock::One->new(
        'X-Mock-ISA' => 'Zaaksysteem::API::Client',
        get => sub {
            $last_get_request = [ @_ ];
            return Zaaksysteem::API::Response->new(
                request_id => '123',
                data => $set_hash,
                http_status => 200
            );
        }
    );

    my $model = Zaaksysteem::OLO::Model->new(
        api_client => $mock_api,
        casetype_id => create_uuid_as_string(UUID_V4),
        attribute_name_map => {
            aanvraagnummer => 'foo',
            bijlagen => 'bar'
        },
        repository => Zaaksysteem::OLO::Repository::Local->new(
            path => "$FindBin::Bin/data/repo"
        )
    );

    isa_ok $model, 'Zaaksysteem::OLO::Model', 'olo model constructor retval';

    # Grab our create test message
    my ($create_message) = grep {
        $_->stuf_reference eq '6584064'
    } $model->repository->get_messages;

    my $exception = exception { $model->process_message($create_message) };

    isa_ok $exception, 'BTTW::Exception::Base',
        'exception thrown by process_message';

    is $exception->type, 'olo/sync/process_create/case_found', 'case exists exception thrown';
}

sub test_olo_create_message_multiple {
    my $last_get_request;

    my $case_a_hash = {
        type => 'case',
        reference => 'f08b7a09-3945-42ac-a620-9d1d3f963868',
        instance => {
            id => 'f08b7a09-3945-42ac-a620-9d1d3f963868',
            attributes => {
                foo => ['abc']
            }
        }
    };

    my $case_b_hash = {
        type => 'case',
        reference => 'f08b7a09-3945-42ac-a620-9d1d3f963868',
        instance => {
            id => 'f08b7a09-3945-42ac-a620-9d1d3f963868',
            attributes => {
                foo => ['abc']
            }
        }
    };

    my $set_hash = {
        type => 'set',
        instance => {
            rows => [ $case_a_hash, $case_b_hash ]
        }
    };

    my $mock_api = Test::Mock::One->new(
        'X-Mock-ISA' => 'Zaaksysteem::API::Client',
        get => sub {
            $last_get_request = [ @_ ];
            return Zaaksysteem::API::Response->new(
                request_id => '123',
                data => $set_hash,
                http_status => 200
            );
        }
    );

    my $model = Zaaksysteem::OLO::Model->new(
        api_client => $mock_api,
        casetype_id => create_uuid_as_string(UUID_V4),
        attribute_name_map => {
            aanvraagnummer => 'foo',
            bijlagen => 'bar'
        },
        repository => Zaaksysteem::OLO::Repository::Local->new(
            path => "$FindBin::Bin/data/repo"
        )
    );

    isa_ok $model, 'Zaaksysteem::OLO::Model', 'olo model constructor retval';

    # Grab our create test message
    my ($create_message) = grep {
        $_->stuf_reference eq '6584064'
    } $model->repository->get_messages;

    my $exception = exception { $model->process_message($create_message) };

    isa_ok $exception, 'BTTW::Exception::Base',
        'exception thrown by process_message';

    is $exception->type, 'olo/sync/multiple_cases_found', 'multiple cases exception type';
}

1;

__END__

=head1 COPYRIGHT and LICENSE

Copyright (c) 2017, Mintlab B.V. and all the persons listed in the L<CONTRIBUTORS|Zaaksysteem::CONTRIBUTORS> file.

Zaaksysteem uses the EUPL license, for more information please have a look at the L<LICENSE|Zaaksysteem::LICENSE> file.
