FROM registry.gitlab.com/zaaksysteem/zaaksysteem-service-http-library:5d35716f

ENV SERVICE_HOME /opt/zaaksysteem-olo-sync-service

WORKDIR $SERVICE_HOME

ENV ZSVC_LOG4PERL_CONFIG=/etc/zaaksysteem-olo-sync-service/log4perl.conf \
    ZSVC_SERVICE_CONFIG=/etc/zaaksysteem-olo-sync-service/zaaksysteem-olo-sync-service.conf

COPY Makefile.PL ./
COPY bin ./bin
COPY etc /etc/zaaksysteem-olo-sync-service
COPY lib ./lib
COPY t ./t
COPY xt ./xt

RUN    cpanm --installdeps . \
    && rm -rf ~/.cpanm \
    || (cat ~/.cpanm/work/*/build.log && /usr/bin/false)

CMD starman $SERVICE_HOME/bin/zaaksysteem-olo-sync-service.psgi
